<p align="center">
  <img src="https://github.com/bootstrap-vue/bootstrap-vue/raw/master/static/banner.png" width="300">
</p>
<br>


<h2 align="center">License</h2>

Released under the MIT [License](./LICENSE). Copyright (c) BootstrapVue.

